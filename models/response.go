package models

import (
	"fmt"
	"time"
)

// Response - A model to handle request's responses
type Response struct {
	JsonRPC string `json:"jsonrpc"`
	ID      uint   `json:"id"`
	Result  Result `json:"result"`
}

// ToText - Make a multi-line string from the response object
func (res *Response) ToText() string {
	// Convert response to a multi-line text
	text := "----------------------------------------\n"
	text += "Date: " + time.Now().String() + "\n"
	text += "Word: " + res.Result.Word + "\n"
	text += "Language:" + res.Result.Lang + "\n"
	text += "Mean:\n"
	// Append each mean of result's means to the text
	for index, mean := range res.Result.Mean {
		text += fmt.Sprintf("%d. %s\n", index, mean)
	}
	// Return text string
	return text
}

// Print - Print response object on the screen
func (res *Response) Print() {
	// Get full text of the object
	text := res.ToText()
	// Print the text on the screen
	fmt.Println(text)
}
